#-*- coding: utf8 -*- 
# main.py
"""

Implementacia problemu obchodneho cestujuceho (TSP) v 3D

"""

import sys, time
#from math import sqrt
import math
from pygene.gene import FloatGene, FloatGeneMax, FloatGeneRandom
from pygene.organism import Organism, MendelOrganism
from pygene.population import Population

import pylibmc
import pp

print "connecting to memcache at 158.196.32.205:11100"
mc = pylibmc.Client(['158.196.32.205:11100'])#,binary=True,behaviors={"tcp_nodelay": True})
print "ok"
"""mc["y"] = "12"
try:
	ip=mc["PC"]
	print ip
except KeyError:
	print 'err'
	pass"""

counter = mc.get("PPSERVER.COUNTER")
print "ppservers available: "+str(counter)
ppservers=()
i=0
print "fetching ppserver adresses and ports"
while i<int(counter):
	ip = str(mc.get("PPSERVER"+str(i)+"IP"))
	port  = str(mc.get("PPSERVER"+str(i)+"PORT"))
	ppservers=ppservers+(ip+":"+port,)
	i+=1
print "ok"
print ""
print "connecting to all nodes"
job_server = pp.Server(ncpus=int(counter),ppservers=ppservers,secret="kluc")
print "ok - starting pp with", job_server.get_ncpus(), "workers"


"""
Pocet generacii genetickeho algoritmu
"""

"""
Konfiguracia
"""
numGenerations=int(sys.argv[6])
geneRandMin = float(sys.argv[8])
geneRandMax = float(sys.argv[9])
geneMutProb = float(sys.argv[10])

"""
Ak sa nepouziva FloatGeneRandom
"""
geneMutAmt = .5        

popInitSize = int(sys.argv[11])
popChildCull = int(sys.argv[14])
popChildCount = int(sys.argv[13])

"""
Pocet rodicov, ktory sa budu pridavat do novej generacie
"""
popIncest = int(sys.argv[15])      

"""
Pravdepodobonost mutacie
"""
popNumMutants = float(sys.argv[12])  

"""
Pocet nahodnych organizmov pridanych do kazdej generacie
"""
popNumRandomOrganisms = int(sys.argv[7])
mutateOneOnly = False
BaseGeneClass = FloatGene
BaseGeneClass = FloatGeneMax
#BaseGeneClass = FloatGeneRandom
OrganismClass = MendelOrganism
mutateAfterMating = True
"""
Pravdepodobnost krizenia
"""
crossoverRate = float(sys.argv[16])

"""
Kazdy gen reprezentuje prioritu cesty do daneho mesta
"""
class CityPriorityGene(BaseGeneClass):

    randMin = geneRandMin
    randMax = geneRandMax
    mutProb = geneMutProb
    mutAmt = geneMutAmt

def parallel_dist(x,y,z,x1,y1,z1):
	dx = x - x1
	dy = y - y1
	dz = z - z1
	return math.sqrt(dx * dx + dy * dy + dz * dz)

"""
Trieda reprezentujuca jedno mesto jeho poradovym cislom a suradnicami.
Navyse pocita vzialenost z ineho mesta.
"""
class City:
    
    """
    Konstruktor vytvori objekt mesta
    """
    def __init__(self, order, x=None, y=None, z=None):
        self.order = order
        self.x = x
        self.y = y
        self.z = z 
        
    """
    Pocita vzdialenost z tohto mesta do ineho 
    """
    def __sub__(self, other):
        dx = self.x - other.x
        dy = self.y - other.y
        dz = self.z - other.z
        return math.sqrt(dx * dx + dy * dy + dz * dz)
        

    def __repr__(self):
        return "<City %s at (%.2f, %.2f, %.2f)>" % (self.order, self.x, self.y, self.z)

cities = [] 
citycounter=0
counter=0
x=1.1
y=2.2
z=3.3

f = open(sys.argv[1],'r')
print "reading input file: "+str(sys.argv[1])
for line in f.readlines():
	counter+=1
	if counter == 1:
		x=float(line)
	elif counter == 2:
		y=float(line)
	elif counter == 3:
		z=float(line)
	elif counter == 4:
		cities.append(City("%s"%citycounter,float(x),float(y),float(z)))
		citycounter+=1
		counter=0
		
f.close()


cityOrders = [city.order for city in cities]

cityCount = len(cities)
print "ok - number of cities: "+str(cityCount)
cityDict = {}
for city in cities:
    cityDict[city.order] = city

priInterval = (geneRandMax - geneRandMin) / cityCount
priNormal = []
for i in xrange(cityCount):
    priNormal.append(((i+0.25)*priInterval, (i+0.75)*priInterval))

genome = {}
for order in cityOrders:
    genome[order] = CityPriorityGene

"""
Jeden pseudo organizmus reprezentuje riesenie problemu
obchodneho cestujuceho
"""
class TSPSolution(OrganismClass):
	genome = genome
	mutateOneOnly = mutateOneOnly
	crossoverRate = crossoverRate
	numMutants = 0.3
	
	"""
	Vracia vzdialenost celej cesty - fitness hodnotu problemu
	"""
	def fitness(self):
		distance = 0.0
		"""
		Ziskanie objektov miest podla priority
		"""
		sortedCities = self.getCitiesInOrder()
		
		"""
		Zaciname prvym mestom a pocitame vzdialenosti k poslednemu
		"""
		 
		distances = []
		
		#job1 = job_server.submit(arallel_dist, (sortedCities[1].x,sortedCities[1].y,sortedCities[1].z,sortedCities[2].x,sortedCities[2].y,sortedCities[2].y), (), ("math",))
		#print job1
		#print "submitting calculations to ppservers"
		for i in xrange(cityCount - 1):
			distances.append(job_server.submit(parallel_dist, (sortedCities[i].x,sortedCities[i].y,sortedCities[i].z,sortedCities[i+1].x,sortedCities[i+1].y,sortedCities[i+1].z), (), ("math",)))
		distances.append(job_server.submit(parallel_dist, (sortedCities[0].x,sortedCities[0].y,sortedCities[0].z,sortedCities[-1].x,sortedCities[-1].y,sortedCities[-1].z), (), ("math",)))
		#distance += sortedCities[i] - sortedCities[i+1]
		#job_server.wait()
		#print "calculated:"
		#for distance in distances:
		#	print distance()
		part_sum1 = sum([distance() for distance in distances])
		
		"""
		Pripocitame cestu k prvemu mestu (navrat)
		"""
		#distance += sortedCities[0] - sortedCities[-1]
		part_sum1 += sortedCities[0] - sortedCities[-1]
		return part_sum1

	
	"""
	Vracia zoznam miest zoradenych podla hodnot
	priorit ziskanych z genov jednotlivych organizmov
	"""
	def getCitiesInOrder(self):
		sorter = [(self[order], cityDict[order]) for order in cityOrders]
		"""
		Zoradenie
		"""
		sorter.sort()
		"""
		Ziskanie jednotlivych miest
		"""
		sortedCities = [tup[1] for tup in sorter]
		return sortedCities
	
	
	"""
	Upravuje geny
	"""
	def normalise(self):
		genes = self.genes
		for i in xrange(2):
			sorter = [(genes[order][i], order) for order in cityOrders]
			sorter.sort()
			sortedGenes = [tup[1] for tup in sorter]
            

"""
Objekt populacie organizmov
"""
class TSPSolutionPopulation(Population):
    initPopulation = popInitSize
    species = TSPSolution
    childCull = popChildCull
    """
    Pocet potomkov, ktory sa maju vytvorit po kazdej populacii
    """
    childCount = popChildCount
    """
    "incest" - Pocet najlepsich rodicov, ktory
    sa pridaju do nasledujucej generacie
    """
    incest = popIncest
    mutants = popNumMutants
    numNewOrganisms = popNumRandomOrganisms
    mutateAfterMating = mutateAfterMating
    
    def evolve(self, nfittest=None, nchildren=None):
        """
        Executes a generation of the population.
        
        This consists of:
            - producing 'nchildren' children, parented by members
              randomly selected with preference for the fittest
            - culling the children to the fittest 'nfittest' members
            - killing off the parents, and replacing them with the
              children
    
        Read the source code to study the method of probabilistic
        selection.
        """
        super(TSPSolutionPopulation, self).gen(nfittest,nchildren)
        




"""
Hlavna metoda programu
"""
def main():
	pop = TSPSolutionPopulation()
	i = 0
	f = open(str(sys.argv[2]),'w')
	f1 = open(str(sys.argv[3]),'w')
	f2 = open(str(sys.argv[5]),'w')
	f3 = open(sys.argv[4],'w')
	while i<=100:
		pop.gen()
		
		i += 1
		"""
		f.truncate()
		f.write(str(i)+"\n")
		f.flush()
		
		
		
		
		f1.truncate()
		f1.write(str(pop.best().fitness())+"\n")
		f1.flush()
		
		
		solution = pop.best()
		sortedCities = solution.getCitiesInOrder()
		"""
		print pop.best().fitness()
		"""
		f3.write("[\n")
		for city in sortedCities:
			f3.write(str(city.order)+"\n")
		f3.write(str(sortedCities[0].order)+"\n")
		f3.write("]\n")
		f3.flush()
		
		
		
		f2.truncate()
		f2.write(str(len(pop))+"\n")
		f2.flush()
		"""
		
	f.close()
	f1.close()
	f3.close()
	f2.close()

if __name__ == '__main__':
	main()
	job_server.print_stats()


